﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductAppClassArray.Model
{
    internal class Product
    {
        #region Declaration
        //int id;
        int id;
        string name;
        string category;
        int price;


        #endregion
        public int Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public int Price { get; set; }


        public Product(int id,string name, string category, int price)
        {
            this.Id = id;
            this.Name = name;
            this.Category = category;
            this.Price = price;
        }

        public override string ToString()
        {
            return $"Id:{this.Id}\t Name:{this.Name}\t Category:{this.Category}\t Price:{this.Price} ";
        }

    }
   
}