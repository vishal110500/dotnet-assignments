﻿using ProductAppClassArray.Model;

namespace ProductAppClassArray.Repository
{
    internal class ProductRepository
    {
        Product[] productArray;

        public ProductRepository()
        {
            productArray = new Product[] 
            {
                new Product(1,"Oppo","Mobile",10000),
                new Product(2,"Vivo","Mobile",12000),
                new Product(3,"Samsung","Tv",40000),
                new Product(4,"Sony","Camera",70000),
                new Product(5,"Sony","Tv",50000),
            };
        }

        public Product[] GetProductsByName(string prodName)
        {
            return Array.FindAll(this.productArray,item=>item.Name==prodName ); 
        }


        public Product[] GetAllProducts()
        {
            return productArray;
        }

        public void UpdateProduct(int Id, string name, string category, int price)
        {
            for(int i = 0; i < productArray.Length; i++)
            {
                if (productArray[i].Id == Id)
                {
                    productArray[i].Name = name;
                    productArray[i].Category = category;
                    productArray[i].Price = price;
                }

            }
        }

        public void DeleteProduct(int id)
        {
            for (int i = 0; i < productArray.Length; i++)
            {
                if (productArray[i].Id != id)
                {
                    productArray[i] = productArray[i];
                }
                else
                {
                    productArray[i] = null;
                }
            }
        }



        //public void AddProduct(int Id,string Name, string Category, int Price)
        //{
        //    productArray = new Product[] { new Product (Id, Name, Category, Price)};
        //}

    }
}
