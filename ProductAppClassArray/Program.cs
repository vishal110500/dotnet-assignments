﻿using ProductAppClassArray.Model;
using ProductAppClassArray.Repository;

ProductRepository productRepository = new ProductRepository();

//Get all the Products
Product[] allproducts = productRepository.GetAllProducts();
foreach (Product item in allproducts)
{
    Console.WriteLine($"{item}");
}

Console.WriteLine("*********************************************");

//Get Products By Name
Console.WriteLine("Enter the product name by which you want to filter");
string prodName = Console.ReadLine();
Product[] filteredArray = productRepository.GetProductsByName(prodName);
foreach (Product item in filteredArray)
{
    Console.WriteLine($"{item}");
}

Console.WriteLine("***********************************************");

//Updating a Product
Console.WriteLine("Enter the Product id which you want to update");
int id = int.Parse(Console.ReadLine());
Console.WriteLine("Enter the new Product name");
string name = Console.ReadLine();
Console.WriteLine("Enter the Product Category");
string category = Console.ReadLine();
Console.WriteLine("Enter the Product Price");
int price = int.Parse(Console.ReadLine());
productRepository.UpdateProduct(id,name,category,price);
//Printing the Updated Product Array
foreach (Product item in allproducts)
{
    Console.WriteLine($"{item}");
}

Console.WriteLine("***********************************************");

//Deleting a Product
Console.WriteLine("Enter the Product id which you want to delete");
int id1 = int.Parse(Console.ReadLine());
productRepository.DeleteProduct(id1);

//Printing the Updated Product Array after deleting the item
foreach (Product item in allproducts)
{
    Console.WriteLine($"{item}");
}





