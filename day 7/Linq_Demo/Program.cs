﻿using Linq_Demo;

List<Product> productList = new List<Product>()
{
    new Product(){Id=1,Name="Apple",Category="Mobile",Price=100000},
    new Product(){Id=2,Name="Samsung",Category="TV",Price=50000},
    new Product(){Id=3,Name="Realme",Category="Mobile",Price=15000},
    new Product(){Id=4,Name="Sony",Category="Camera",Price=58000},
};

List<Product1> productList1 = new List<Product1>()
{
    new Product1() { Name = "Apple", Quantity = 5 },
    new Product1() { Name = "Samsung", Quantity = 2 },
    new Product1() { Name = "Realme", Quantity =3 },
    //new Product1() { Name = "Apple", Quantity = 7}
};


//Join Operation using LINQ

var result = from p in productList
             join p1 in productList1
             on p.Name equals p1.Name
             select new
             {
                productid = p.Id,
                productName = p.Name,
                productquantity = p1.Quantity,
                 
             };

foreach (var item in result)
{
    Console.WriteLine($"{item.productid}\t{item.productName}\t{item.productquantity}");
}






