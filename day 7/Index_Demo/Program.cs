﻿
using Indexer_Demo;

Product product = new Product(1,"Apple","Mobile");
Console.WriteLine($"Id::{product["First"]}\t Name::{product["Second"]}\t Category::{product["Third"]}");
product["First"] = 2;
Console.WriteLine($"Id::{product["First"]}\t Name::{product["Second"]}\t Category::{product["Third"]}");