﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Indexer_Demo
{
    internal class Product
    {
        private int id;
        private string name;
        private string category;

        public Product(int id, string name,string category)
        {
            this.id = id;
            this.name = name;
            this.category = category;
        }
        
        public object this[string index]
        {
            get
            {
                if (index == "First") return id;
                else if (index == "Second") return name;
                else if (index == "Third") return category;
                else return null;
            }
            set
            {
                if (index == "First") id = (int)value;
                else if (index == "Second") name = (string)value;
                else if (index == "Third") category =(string) value;

            }
        }


    }
}
