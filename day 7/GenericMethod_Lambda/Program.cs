﻿namespace GenericMethod_Lambda
{
    class Program
    {
        public static void Main()
        {
            Func<int,int,int> ds = (x, y) => x * y;
            int result = ds(10, 20);
            Console.WriteLine(result);


            Action<int,int> ds1 = ((x, y) => Console.WriteLine(x * y));
            ds1(10, 12);


            Predicate<string> dc = (x => x.Count() > 4?true:false);
            bool resultOfCount = dc("Delegates");
            Console.WriteLine(resultOfCount);

            //int Square(int x,int y)
            //{
            //    return x * y;
            //}

            //void Square1(int x,int y)
            //{
            //    Console.WriteLine(x*y);
            //}

            //bool CountofString(string Z)
            //{
            //    bool result;
            //    if (Z.Count() > 4)
            //    {
            //        result = false;
            //    }
            //    else 
            //    { 
            //        result = true; 
            //    }
            //    return result;
            //}
            

        }
    }
}


//Func Delegate----------when fuction returns a value, go for Func Delegate
//Action Delegate--------whenever function does not return a value, use Action Delegate
//Predicate Delegate-----bool return types, go for Predicate Delegate
