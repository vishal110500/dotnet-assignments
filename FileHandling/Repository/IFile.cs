﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHandling.Model;

namespace FileHandling.Repository
{
    internal interface IFile
    {
        void AddContentsToFile(User user,string fileName);

        bool IsUserExist(string userName,string fileName);    

        List<string> ReadContentFromFile(string fileName);
    }
}
