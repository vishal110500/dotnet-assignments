﻿using FileHandling.Exceptions;
using FileHandling.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling.Repository
{
    internal class UserRepository : IUserRepository, IFile
    {
        List<User> userList;
        
        public UserRepository()
        {
            userList = new List<User>(); 
        }

        public void AddContentsToFile(User user,string fileName)
        {
            using (StreamWriter sw = new StreamWriter(fileName, true))
            {
                sw.Write(user);
            }
             
  
        }

        public bool IsUserExist(string userName,string fileName)
        {
            bool isUserAvailable = false;
            using (StreamReader sr = new StreamReader(fileName))
            {
                string rowline;
                while ((rowline = sr.ReadLine())!= null)
                {
                   string[] rowlist = rowline.Split(",");
                    foreach (string items in rowlist)
                    {
                        if (items == userName)
                        {
                            //isUserAvailable = true;
                            throw new InvalidUser("User already exist");
                            //break;
                        }
                    }

                    //if (rowlist[1] == username)
                    //{
                    //    isuseravailable = true;
                    //    break;
                    //}
                }
            }
            return isUserAvailable;

        }

        public List<string> ReadContentFromFile(string fileName)
        {
            List<string> rowValues = new List<string>();
            using (StreamReader sr = new StreamReader(fileName))
            {
                string rowline;
                while ((rowline = sr.ReadLine()) != null)
                {
                    rowValues.Add(rowline);
                }
                return rowValues; 
            }
        }

        public bool RegisterUser(User user)
        {
            userList.Add(user);
            string fileName = "userDataBase.txt";
            bool isUserNameExist=IsUserExist(user.Name,fileName);
            if (isUserNameExist)
            {
                return false;
            }
            else
            {
                AddContentsToFile(user, fileName);
                return true;

            }

        }
    }
}
