﻿using FileHandling.Repository;
using FileHandling.Model;
using FileHandling.Exceptions;

//Instance of Class User
User user = new User();

//Taking the Input from the user
Console.WriteLine("Register..............");

Console.WriteLine("Id::");
user.Id = int.Parse(Console.ReadLine());

Console.WriteLine("Name::");
user.Name = Console.ReadLine();

Console.WriteLine("City::");
user.City = Console.ReadLine();



UserRepository userRepository = new UserRepository();

IUserRepository iuserRepository = (IUserRepository) userRepository; //Cascading


try
{
    bool isUserRegistered = iuserRepository.RegisterUser(user);
   
    if (isUserRegistered)
    {
        Console.WriteLine("User Registered Successfully");
    }
}
catch (InvalidUser ex)
{
    Console.WriteLine(ex.Message);
}

//    bool isUserRegisterd = iuserRepository.RegisterUser(user);
//    if (isUserRegisterd)
//    {
//        Console.WriteLine("User Registered Successfully");
//    }
//}

//else
//{
//  Console.WriteLine("User already Exist")
//}


Console.WriteLine("Reading from file");

List<string> userContent = userRepository.ReadContentFromFile("userDataBase.txt");
foreach (string filedata in userContent)
{
    Console.WriteLine(filedata);
}



